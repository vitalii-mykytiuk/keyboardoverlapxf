﻿using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;

namespace KeyboardOverlapXF
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            On<iOS>().SetUseSafeArea(true);
            List<string> list = new List<string>();
            for (int i = 0; i < 50; i++)
            {
                list.Add(string.Empty);
            }
            collectionView.ItemsSource = list;
            Xamarin.Forms.Application.Current.On<Android>().
            UseWindowSoftInputModeAdjust(WindowSoftInputModeAdjust.Resize);
        }
    }
}
